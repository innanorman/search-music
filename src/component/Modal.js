import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom';

const ModalSearch = ({open, setModalOpen}) => {
	const [search, setSearch] = useState()
	const navigate = useNavigate();
	const clickSearchModal = () => {
		navigate("/results/" + search)
		setModalOpen(false)
	}

	return (
		<div className={`modal ${open ? 'bold' : ''}`}>
			<div className='modal-overlay'>
				<input 
					className='input'
					name="searchInput" 
					placeholder='Artist / Album / Title' 
					onChange={(e) => setSearch(e.target.value)}
				/>
				<div style={{width: "100%"}}>
					<button className="button-search button-modal" onClick={clickSearchModal}>Search</button>
				</div>
			</div>	
		</div>
	)
}

export default ModalSearch