import React, { useState } from 'react'
import { Link } from 'react-router-dom';

const SearchInput= () => {
	const [search, setSearch] = useState()

	return (
		<div className='search-button'>
			<input
				className='input' 
				name="searchInput" 
				placeholder='Artist / Album / Title' 
				onChange={(e) => setSearch(e.target.value)}
			/>
			<div style={{width: "100%"}}>
				<Link to={{ pathname: "/results/" + search}} >
					<button className="button-search">Search</button>
				</Link>
			</div>
		</div>
	)
}

export default SearchInput