import logo from './logo.svg';
import './App.css';
import { Routes, Route } from 'react-router-dom';
import Home from './pages/Home';
import Result from './pages/Result';

function App() {
	return (
		<div className='wrapper'>
			<Routes>
				<Route path="/" element={<Home/>} />
				<Route path="/results/:id" element={<Result/>}/>
			</Routes>
		</div>
	);
}

export default App;
