import React from 'react'
import Logo from '../assets/logo.svg'
import SearchInput from '../component/SearchInput'

const Home = () => {
  return (
	<div className='homepage'>
		<div className='logo'>
			<img src={Logo} />
		</div>
		<SearchInput />
	</div>
  )
}

export default Home