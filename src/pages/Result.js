import React, { useEffect, useState } from 'react'
import { useLocation, useParams } from "react-router-dom";
import dollarIcon from "../assets/currency-dollar.svg";
import menuIcon from "../assets/menu.svg"
import logoNameIcon from "../assets/ngmusic.svg"
import searchIcon from "../assets/search.svg"
import playCircle from "../assets/play-circle-filled-twotone-24-px.svg"
import ModalSearch from '../component/Modal';
import loadingGif from '../assets/gif-load.gif'


const Result = () => {
	const {id} = useParams();
	const [loading, setLoading] = useState(false)
	const [result, setResult] = useState()
	const [count, setCount] = useState(4)
	const [modalOpen, setModalOpen] = useState(false)

	async function fetchData({count, id}){
		try {
			setLoading(true)
			const params = {
				media: "music",
			}
			const data = await fetch(`https://itunes.apple.com/search?term=${id}&limit=${count}&media=${params.media}`)
			.then((res) => res.json())
			setResult(data)
			setLoading(false)
		} catch{
			setLoading(false);
			console.error('error');
		}
	}



	useEffect(() => {
		fetchData({count, id})
	}, [count, id])

	const increment = () => {
		setCount(count + 4); 
	}

	const clickModal = () => {
		setModalOpen(!modalOpen)
	}


	return (
		<div className='result-page'>
			<div className='result-header'>
				<div className='header-wrapper'>
					<div className='menu'>
						<img src={menuIcon} />
					</div>
					<div className='logo'>
						<img src={logoNameIcon} />
					</div>
					<div className='search' onClick={clickModal}>
						<img src={searchIcon} />
					</div>
				</div>
			</div>
			{modalOpen && 
				<ModalSearch 
					open={modalOpen}
					setModalOpen={setModalOpen}
				/>
			}
			{loading && 
				<div className='loading-center'>
					<img src={loadingGif} />
				</div>
			}
			 
			{result &&
				<div className='result-list'>
					<div className='header-text'>Search Result for : <span className='text-result'>{id}</span> </div>

					{result?.results?.map((item) =>
						<div className='result-list-item'>
							<div className='item-wrapper'>
								<div className='player-track'>
									<div className='audio-wrapper'>
										<img src={playCircle} className='player-icon'/>
										<img src={item.artworkUrl100} />
									</div>
								</div>
								<div className='info-track'>
									<div className='artist-name'>
										{item?.artistName} 
									</div>
									<div className='track-name'>{item?.trackName}</div>
									<div className='extra-section'>
											<div className='genre-section'>{item.primaryGenreName}</div>
											<div className='price-section'>
													<span className='icon'><img src={dollarIcon} /></span><span>{item.trackPrice}</span>
											</div>
									</div>
								</div>
							</div>
						</div>
					)}
					{result?.results?.length > 0 ?
						<button onClick={increment} className='more'>Load more</button> : ''
					}
				</div>
			}   
		</div>
	)
}

export default Result